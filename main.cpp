#include <QCoreApplication>
#include <QtCore>
#include <QFile>

#include <algorythm.h>
#include <datamanager.h>

using namespace std;

int main(int argc, char** argv)
{
	QCoreApplication a(argc, argv);

	Algorythm my("out.txt");

	my.makeFullStep();
	my.makeResult("step.txt", "out.txt");

	return a.exec();
}

