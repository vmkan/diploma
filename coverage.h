#ifndef COVERAGE_H
#define COVERAGE_H

#include <QSet>
#include <fstream>
#include <QTextStream>
#include <QString>

using namespace std;

class Coverage
{
private:
	int ppsLength = 100000;
	int ppsStep = 100;
	int bppLength = 1353;
	int bppStep = 100;

//каждая из переменных храненит значения тестируемых параметров, количество элементов указывается вручную
public:
	QSet<int> packetsPerSecond;
	QSet<int> bytesPerPacket;
	QSet<int> sessions;

	Coverage();
	Coverage(QString file);
};

class Test
{
private:
	int packetsPerSecond;
	int bytesPerPacket;
	int sessions;
public:
	Test(int pps, int bpp, int pSessions);
	void setPPS(int pps);
	int getPPS() const;
	void setBPP(int bpp);
	int getBPP() const;
	void setSesssions(int pSessions);
	int getSessions() const;

	bool operator <(const Test& other) const
	{
		return (getPPS() < other.getPPS()) ||
		((getPPS() == other.getPPS()) && (getBPP() < other.getBPP())) ||
		((getPPS() == other.getPPS()) && (getBPP() == other.getBPP()) && (getSessions() < other.getSessions()));
	}
};

inline QTextStream& operator<< (QTextStream& output, const Test& a)
{
	output << a.getBPP() << " " << a.getPPS() << " " << a.getSessions();
	return output;
}

#endif // COVERAGE_H
