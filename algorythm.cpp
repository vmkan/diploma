#include <QDebug>
#include <QFile>
#include <QDateTime>
#include <QStringList>

#include "algorythm.h"
#include <ctime>

#define GBIT 125000000
#define RANDOMTESTS 6000
#define FULLSTEP 6

using namespace std;

Algorythm::Algorythm():
			iterPPS(main.packetsPerSecond),
			iterBPP(main.bytesPerPacket),
			iterSessions(main.sessions)
{
	main  = Coverage();
}

Algorythm::Algorythm(QString file):
			iterPPS(main.packetsPerSecond),
			iterBPP(main.bytesPerPacket),
			iterSessions(main.sessions)
{
	main  = Coverage(file);
}

void Algorythm::makeFull()
{
	iterPPS = main.packetsPerSecond;
	iterBPP = main.bytesPerPacket;
	iterSessions = main.sessions;
	QFile* os = new QFile("full.txt");
	QTextStream out(os);
	os->open(QIODevice::WriteOnly);
	int pps = iterPPS.next();
	int bpp = iterBPP.next();
	while(iterPPS.hasNext())
	{
		if(pps * bpp <= GBIT)
			out << pps << " " << bpp << " " << iterSessions.next() << endl;
		else
			iterSessions.next();

		if(!iterSessions.hasNext())
		{
			iterSessions = main.sessions;
			bpp = iterBPP.next();
		}

		if(!iterBPP.hasNext())
		{
			iterBPP =  main.bytesPerPacket;
			pps = iterPPS.next();
		}
	}
	os->close();
	cout << "makeFull done." << endl;
}

int Algorythm::find(QList<QPair<int, int>> list, int value)
{
	int i = 0;
	for(auto x : list)
		if(x.first == value)
			return i;
	return -1;
}

bool Algorythm::findTest(Test test, QList<Test> where)
{
	for(Test x : where)
	{
		if((x.getBPP() == test.getBPP()) &&
			(x.getPPS() == test.getPPS()) &&
			(x.getSessions() == test.getSessions()))
			return true;
	}

	return false;
}

void Algorythm::makeResult(QString testFile, QString mainFile)
{
	QFile testInputFile(testFile);
	QFile mainInputFile(mainFile);
	QTextStream testInputStream(&testInputFile);
	QTextStream mainInputStream(&mainInputFile);
	testInputFile.open(QIODevice::ReadOnly);
	mainInputFile.open(QIODevice::ReadOnly);
	QMap<Test, double> main;
	QList<Test> tests;

	while (!testInputStream.atEnd())
	{
		QString line = testInputStream.readLine();
		QStringList cortege = line.split(" ", QString::SkipEmptyParts);
		int bpp = cortege[1].toInt();
		int pps = cortege[0].toInt();
		int sess = cortege[2].toInt();
		tests.push_back(Test(pps, bpp, sess));
	}

	while (!mainInputStream.atEnd())
	{
		QString line = mainInputStream.readLine();
		QStringList cortege = line.split(" ", QString::SkipEmptyParts);
		int bpp = cortege[0].toInt();
		int pps = cortege[1].toInt();
		int sess = cortege[2].toInt();
		double delay = cortege[3].toDouble();
		main[Test(bpp, pps, sess)] = delay;
	}

	testInputFile.close();
	mainInputFile.close();

	QFile answerFile("answer.txt");
	QTextStream answerStream(&answerFile);
	QFile delayFile("delays.txt");
	QTextStream delayStream(&delayFile);
	answerFile.open(QIODevice::WriteOnly);
	delayFile.open(QIODevice::WriteOnly);
	QList<double> pairDelays;

	foreach(Test x, tests)
	{
		answerStream << x << " " << main[x] << "\n";
		pairDelays.push_back(main[x]);
	}

	qSort(pairDelays);
	for(double d : pairDelays)
		delayStream << d << "\n";

	delayFile.close();
	answerFile.close();
}

void Algorythm::makePair()
{
	set<Test> res;
	QList<QPair<int, int>> bppPPS;
	QList<QPair<int, int>> ppsSess;

	iterPPS = main.packetsPerSecond;
	iterBPP = main.bytesPerPacket;
	iterSessions = main.sessions;

	while(iterBPP.hasNext())
	{
		while(iterPPS.hasNext())
			bppPPS.push_back(qMakePair(iterBPP.peekNext(),iterPPS.next()));
		iterBPP.next();
		iterPPS = main.packetsPerSecond;
	}
	iterBPP = main.bytesPerPacket;

	while(iterPPS.hasNext())
	{
		while(iterSessions.hasNext())
			ppsSess.push_back(qMakePair(iterPPS.peekNext(),iterSessions.next()));
		iterPPS.next();
		iterSessions = main.sessions;
	}
	iterPPS = main.packetsPerSecond;

	qsrand (QDateTime::currentMSecsSinceEpoch());

	if(!ppsSess.isEmpty())
	{
		QList<QPair<int, int>>::iterator iter = ppsSess.begin();
		while(iter != ppsSess.end())
		{
			//qDebug() << iter->first << " " << iter->second;
			int pos = find(bppPPS, iter->first);
			if(pos != -1)
				res.insert(Test(bppPPS[pos].first, iter->first, iter->second));
			else
				res.insert(Test(
					main.bytesPerPacket.toList()[qrand() % main.bytesPerPacket.size()],
					   iter->first, iter->second));
			bppPPS.removeAt(pos);
			iter = ppsSess.erase(iter);
		}
	}

	if(!bppPPS.isEmpty())
	{
		QList<QPair<int, int>>::iterator iter = bppPPS.begin();
		while(iter != bppPPS.end())
		{
			int pos = find(ppsSess, iter->first);
			if(pos != -1)
				res.insert(Test(ppsSess[pos].first, iter->first, iter->second));
			else
				res.insert(Test(
					main.bytesPerPacket.toList()[qrand() % main.bytesPerPacket.size()],
					   iter->first, iter->second));
			ppsSess.removeAt(pos);
			iter = bppPPS.erase(iter);
		}
	}

	QFile* os = new QFile("pair.txt");
	QTextStream out(os);
	os->open(QIODevice::WriteOnly);
	for(Test x : res)
		out << x << "\n";

	os->close();
	cout << "makePair done." << endl;
}

void Algorythm::makeFullStep()
{
	iterPPS = main.packetsPerSecond;
	iterBPP = main.bytesPerPacket;
	iterSessions = main.sessions;
	QList<Test> full;
	while(iterPPS.hasNext())
	{
		while(iterBPP.hasNext())
		{
			while(iterSessions.hasNext())
				full.push_back(Test(iterPPS.peekNext(), iterBPP.peekNext(), iterSessions.next()));
			iterSessions = main.sessions;
			iterBPP.next();
		}
		iterBPP = main.bytesPerPacket;
		iterPPS.next();
	}

	QFile* os = new QFile("step.txt");
	QTextStream fullStream(os);
	os->open(QIODevice::WriteOnly);

	for(int i = 0; i <= full.size() - FULLSTEP; i += FULLSTEP)
		fullStream << full[i] << "\n";

	os->close();
	cout << "makeFullWithStep done." << endl;
}

void Algorythm::makeRandom()
{
	srand(time(0));
	set<Test> res;
	int rPPS, rBPP, rSession;
	int pSize = main.packetsPerSecond.size();
	int bSize = main.bytesPerPacket.size();
	int sSize = main.sessions.size();

	for(int i = 0; i < RANDOMTESTS; i++)
	{
		do
		{
			rPPS = rand() % pSize;
			rBPP = rand() % bSize;
			rSession = rand() % sSize;
		}while(res.count(Test(main.packetsPerSecond.toList()[rPPS],
				      main.bytesPerPacket.toList()[rBPP],
				      main.sessions.toList()[rSession])));

		res.insert(Test(main.packetsPerSecond.toList()[rPPS],
				main.bytesPerPacket.toList()[rBPP],
				main.sessions.toList()[rSession]));
	}

	QFile* os = new QFile("fullrand.txt");
	QTextStream out(os);
	os->open(QIODevice::WriteOnly);
	for(Test x : res)
		out << x << "\n";

	os->close();

	cout << "makeRandom done." << endl;
}
