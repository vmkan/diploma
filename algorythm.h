#ifndef ALGORYTHM_H
#define ALGORYTHM_H

#include <QFile>
#include <QSet>
#include <QSetIterator>
#include "coverage.h"
#include <set>
#include <iostream>
#include <fstream>

using namespace std;

class Algorythm
{
private:
	Coverage main;
	QSetIterator<int> iterPPS;
	QSetIterator<int> iterBPP;
	QSetIterator<int> iterSessions;
	int find(QList<QPair<int, int>> list, int value);
	bool findTest(Test test, QList<Test> where);
public:
	Algorythm();
	Algorythm(QString file);
	void makeResult(QString pairFile, QString mainFile);
	void makeFull();
	void makePair();
	void makeFullStep();
	void makeRandom();

};

#endif // ALGORYTHM_H
