#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QFileInfo>
#include <QDebug>

#include "coverage.h"

Coverage::Coverage()
{
	for(int i = ppsStep; i <= ppsLength; i += ppsStep)
		packetsPerSecond.insert(i);

	for(int i = bppStep; i <= bppLength; i += bppStep)
		bytesPerPacket.insert(i);

	sessions.insert(1);
	sessions.insert(16);
	sessions.insert(128);
	sessions.insert(512);
	sessions.insert(1024);
}

Coverage::Coverage(QString file)
{
	QFile input(file);
	input.open(QIODevice::ReadOnly);
	QTextStream inputStream(&input);
	qint64 size = QFileInfo(input).size();
	qint64 readSize = 0;
	while (!inputStream.atEnd())
	{
		QString line = inputStream.readLine();
		QStringList cortege = line.split(" ", QString::SkipEmptyParts);
		int bpp = cortege[0].toInt();
		int pps = cortege[1].toInt();
		int sess = cortege[2].toInt();
		bytesPerPacket.insert(bpp);
		packetsPerSecond.insert(pps);
		sessions.insert(sess);
		readSize += line.size();
		qDebug() << readSize << " of  " << size;
	}
	input.close();
}

Test::Test(int pps, int bpp, int pSessions)
{
	packetsPerSecond = pps;
	bytesPerPacket = bpp;
	sessions = pSessions;
}

void Test::setPPS(int pps)
{
	packetsPerSecond = pps;
}

int Test::getPPS() const
{
	return packetsPerSecond;
}

void Test::setBPP(int bpp)
{
	bytesPerPacket = bpp;
}

int Test::getBPP() const
{
	return bytesPerPacket;
}

void Test::setSesssions(int pSessions)
{
	sessions = pSessions;
}

int Test::getSessions() const
{
	return sessions;
}
